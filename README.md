# PHP Security
This application gives an overview of the different between encryption, encoding, and hashing. It provides examples of using the PHP password API bundled into PHP as of 5.5 and avaialble in userland as of PHP 5.3.8. It also provides example code for session security, hijacking sessions, and a paper on PHP security.

Public domain. Assume GPL 2.0 if license is not provided.