<?php require_once 'header.php'; ?>

      <div class='page-header'>
        <h1>Sources</h1>
      </div>

      <div class="row">
        <div class="col-md-12">
          <p>[1] ircmaxell. (2012). <em>password_compat</em>. Available: <a href='https://github.com/ircmaxell/password_compat' target="_blank">https://github.com/ircmaxell/password_compat</a>
          </p>

          <p>[2] PHP.net Developers. (Unknown). <em>Password Hashing</em>. Available: <a href='http://php.net/manual/en/book.password.php' target="_blank">http://php.net/manual/en/book.password.php</a>
          </p>

          <p>[3] S. Doty. (2014, Nov. 10). <em>Encryption vs Hashing, and PHP</em>. Available e-mail: sdoty@luc.edu Message: &hellip;You might add that RSA is based on the difficulty of factoring very large integers. Also, hashing uses the same algorithms as encryption/decryption, but for a different purpose: to create a &quot;digest&quot; of the input, which is a short string that (nearly) uniquely represents it.&hellip;
          </p>

          <p>[4] PHP.net Developers. (Unknown) <em>Session Management Basics</em>. Avaialable: <a href="http://php.net/manual/en/features.session.security.management.php" target="_blank">http://php.net/manual/en/features.session.security.management.php</a></p>
        </div> 
      </div>    


<?php require_once 'footer.php'; ?>