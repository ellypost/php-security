jQuery(document).ready(function($){

	//listen for password hash form submit
    $(document).on('submit', "#frm-password-hash", function(e){
    	e.preventDefault();

    	//get the data
    	$("#pass-mismatch").hide();
    	var pass1 = $.trim( $('.password1', $(this) ).val() );
    	var pass2 = $.trim( $('.password2', $(this) ).val() );
    	if( pass1 != pass2 ) {
    		$("#pass-mismatch").show();
    		$("#frm-password-hash")[0].reset();
            return;
    	}
        var data = $(this).serialize();

        $("#pass-hash-results").html( "Loading results&hellip;" );

        //send the data
        $.post( "actions.php", data, function(response) {
        	//receive and process data as JSON (JavaScript Object Notation)
        	var r = $.parseJSON( response );
            $('html, body').animate({
                scrollTop: ($("#pass-hash-results").offset().top - 50 )
            }, 500);
            $("#pass-hash-results").effect("highlight", {}, 7000);
        	if( r.status == "false" ) {
        		//an error occured
        		$( "#pass-hash-results" ).html("<span class='text-danger'><strong>An error occured:</strong></span><br>" + r.msg);

        	} else {
        		$( "#pass-hash-results" ).html( r.msg + "<br>Code:<br><code>" + r.code + "</code>" + "<br><br>Values:<br><strong>Hash1:</strong> " + r.hash1 + "<br><strong>Hash2:</strong> " + r.hash2 );
        	}
            $("#frm-password-hash")[0].reset();
        });
    });

	//listen for password verify form submit
    $(document).on('submit', "#frm-password-verify", function(e){
    	e.preventDefault();

    	//get the data
        var data = $(this).serialize();
        $("#pass-verify-results").html( "Loading results&hellip;" );

        //send the data
        $.post( "actions.php", data, function(response) {
        	//receive and process data as JSON (JavaScript Object Notation)
        	var r = $.parseJSON( response );
		    $('html, body').animate({
		        scrollTop: ($("#pass-verify-results").offset().top - 50 )
		    }, 500);
            $("#pass-verify-results").effect("highlight", {}, 7000);

        	if( r.status == "false" ) {
        		//an error occured
        		$( "#pass-verify-results" ).html("<span class='text-danger'><strong>An error occured:</strong></span><br>" + r.msg);

        	} else {
        		$( "#pass-verify-results" ).html( r.msg + "<br>Code:<br><code>" + r.code + "</code>" + "<br><br><strong>Matches?</strong> " + r.matches );
        	}
            $("#frm-password-verify")[0].reset();
        });
    });
});