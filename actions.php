<?php

$jsonData = array( "status" => "false", //intentional use of a string for false!
	 "msg" => "No Data Received"
	  );

//ensure we have an action
if( !isset( $_POST['action'] ) || empty( $_POST['action'] ) ) 
	die( json_encode( $jsonData ) );

require_once 'api.password.php';

//what are we doing?
switch( $_POST['action'] ) {
	case "password_hash":
		//get our hashes
		$hash1 = password_hash( $_POST['password1'], PASSWORD_DEFAULT );
		$hash2 = password_hash( $_POST['password2'], PASSWORD_DEFAULT );

		//copy the code for the user
		ob_start();
		?>$hash1 = password_hash( $_POST['password1'], PASSWORD_DEFAULT );
		<br>
		$hash2 = password_hash( $_POST['password2'], PASSWORD_DEFAULT );
		<?php
		$jsonData['code'] = ob_get_contents(); 
		ob_clean();

		$jsonData['status'] = "true";
		$jsonData['hash1'] = $hash1;
		$jsonData['hash2'] = $hash2;
		$jsonData['msg'] = "Hashed password using the below code. Notice the different values between hash1 and hash2, even though the original passwords matched.";

		die( json_encode( $jsonData ) );
		break;


	case "password_verify":

		//be certain to use single quotes here! Double quotes will try to parse this as PHP var
		$savedHash = '$2y$10$5oueQ1RSq1pEP8LXsDX0XefjVZPMzX2fhHKOx8gHpLG9zRqyNWex.';
		$matches = password_verify( $_POST['password1'], $savedHash );

		//copy the code for the user
		ob_start();
		?>//be certain to use single quotes here! Double quotes will try to parse this as PHP var
		<br>
		$savedHash = '$2y$10$5oueQ1RSq1pEP8LXsDX0XefjVZPMzX2fhHKOx8gHpLG9zRqyNWex.';
		<br>
		$matches = password_verify( $_POST['password1'], $savedHash );
		<?php
		$jsonData['code'] = ob_get_contents(); 
		ob_clean();

		$jsonData['status'] = "true";
		$jsonData['matches'] = ( $matches ? "true" : "false" );
		$jsonData['msg'] = "Comparing your password to the saved value. See result below.";

		die( json_encode( $jsonData ) );
		break;
}

//malformed request
$jsonData['msg'] = "Could not understand action: {$_POST['action']}";
die( json_encode( $jsonData ) );