<?php

session_start();

require_once 'api.password.php';

//connect to the DB
$dbName = "php-security";
$dbHost = "localhost";
$dbUser = "php-security";
$dbPass = "cheesecurds2016";

$db = new mysqli( $dbHost, $dbUser, $dbPass, $dbName );
unset( $dbName, $dbHost, $dbUser, $dbPass );

if( $db->connect_errno )
    die( "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error );

if( !$db->set_charset( "utf8mb4" ) ) {
    printf("Error loading character set utf8mb4: %s\n", $db->error);
} 

//try to login
$username = $db->real_escape_string($_POST['username']);
$password = $db->real_escape_string($_POST['password']);
$tablePrefix = "";
$userTable = $tablePrefix . "user";

$query = "SELECT * FROM {$userTable} WHERE username='{$username}'";
$result = $db->query( $query );
if( !$result ) {
	//an error occured
	die( "There was a problem executing the SQL query. MySQL error returned: {$db->error} (Error #{$db->errno})" );
}

if( !$result->num_rows ) {
	//no results found
	die( "This user does not exist." );
}

//get the user as an object
while( $row = $result->fetch_assoc() ) 
	$user = (object) $row;

//free the memory, discard the db query
$result->free();

//verify the passwords match
$matches = password_verify( $password, $user->password );
if( !$matches ) 
	die( "Invalid password." );

$_SESSION['user'] = $user;

?>

<h1>Logged in as user:</h1>
<pre><?php print_r($user);?></pre>

<?php
require_once 'footer-sessions.php';