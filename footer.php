
    </div><!-- /.container -->

    <div class="footer">
      <div class="container">
        <p class="text-muted pull-right">Created by <a href="http://ellytronic.media" target="_blank"><img src="images/transparent-ellytronic-small.png" alt="Ellytronic Media"> Ellytronic Media</a>. This work is open-source and may be modified or distributed freely.</p>
        <div class="clearfix"></div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/c453-primary.js"></script>
  </body>
</html>