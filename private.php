<?php

session_start();

if (!isset($_SESSION['user']))
	die("You must log in first.");

?>

<h1>Hello, <?=$_SESSION['user']->fname?> <?=$_SESSION['user']->lname?></h1>

Here is your account data:
<pre>
<?php var_dump($_SESSION['user']);?>
</pre>

<?php
require_once 'footer-sessions.php';