<?php require_once 'header.php'; ?>

      <div class='page-header'>
        <h1>PHP Password Hashing Demo</h1>
        <p class="lead">
          This file is an example of how to use the PHP Password Hashing functions. 
          <?php
          if( phpversion() < 5.37 ) {
            ?>
            <div class="alert alert-danger" role="alert">
              You are running PHP <?=phpversion();?> which does not support these functions. You must upgrade or the following functions may not perform correctly.
            </div>
            <?php
          } else if ( phpversion() < 5.5 ) 
            require_once 'api.password.php';

          //ensure the functions are working
          if( !function_exists( "password_hash" ) ) {
            ?>
            <div class="alert alert-danger" role="alert">
              Could not load the password hashing functions!
            </div>
            <?php
          } else {
            ?>
            <div class="alert alert-success alert-dismissable" role="alert">
              <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              Success! Loaded the PHP Hashing functions.
            </div>
            <?php
          }

          ?>
        </p>
      </div>

      <div class="row">
        <div class="col-md-12">
          <h2>password_hash()</h2>
          <p>
          <code>password_hash()</code> is the basic function for the password hashing functions. It takes two arugments, the string (password) to hash, and
          the encryption method. I recommend you use <code>PASSWORD_DEFAULT</code> as the encryption method. Currently this defaults to BCRYPT (strongest),
          but if a better algorithm comes out, it will automatically update your application for you. So, your example code looks like 
          <code>password_hash( "my_password", PASSWORD_DEFAULT );</code>.</p>

          <p>One of the most powerful features of hashing is that time effects the hash output. Let's examine this further.</p>
          <div class="alert alert-danger" role="alert" id='pass-mismatch' style='display:none;'>
              Your passwords did not match. Please try again.
          </div>
          <form method="POST" id="frm-password-hash">
            <div class='form-group'>
              <label>Enter a password:</label>
              <input type="text" name="password1" required class="password1 form-control">
            </div>
            <div class='form-group'>
              <label>Confirm the password:</label>
              <input type="text" name="password2" required class="password2 form-control">
            </div>
            <div class='form-group'>
                <input type="hidden" name="action" value="password_hash">
              <button type="submit" class="btn btn-primary" name="action" value="password_hash">Hash My Password!</button>
            </div>
          </form>

          <h4>Results:</h4>
          <div id="pass-hash-results" class='well'>
            <em>Please supply a password first.</em>
          </div>
        </div>   
      </div>

      <div class="row">
        <div class="col-md-12">
          <h2>password_verify()</h2>
          <p>
          <code>password_verify()</code> is the function for verifying a password hash. This is the only way to verify the password is correct because
          no other PHP equality checks ( == OR === ) account for the time factor with the hash. This function takes two arugments, the <em>plain text</em>
          string (password) to hash, and the (saved) hash to compare it to. You would likely get the second argument as a value from a database. 
          So, your example code looks like <code>password_verify( "my_password", $savedHashInDatabase );</code>.</p>

          <p>Let us now compare passwords. Try entering a random password. Then, after submitting, try entering &quot;<em>phpmadison</em>&quot; as your password. (No quotes).</p>
          <form method="POST" id="frm-password-verify">
            <div class='form-group'>
              <label>Enter a password:</label>
              <input type="text" name="password1" required class="form-control">
            </div>
            <div class='form-group'>
              <input type="hidden" name="action" value="password_verify">
              <button type="submit" class="btn btn-primary">Verify My Password!</button>
            </div>
          </form>

          <h4>Results:</h4>
          <div id="pass-verify-results" class='well'>
            <em>Please supply a password first.</em>
          </div>
        </div>   
      </div>

      <div class="row">
        <div class="col-md-12">
          <h2>password_needs_rehash()</h2>
          <p>
          <code>password_needs_rehash()</code> is the function used to determine if it's time to update the hash. This isn't something you need to check for every time,
          but it's a good idea to implement this in your login script such that it runs once a month or something of the like. A password may need a rehash if a hashing
          algorithm changes, your hosting performance changes (the more powerful machine, the stronger the hash), or other reasons. You need to know the original algorithm it was 
          hashed with to use this. I have sample code for this below, but I leave this to you to find an example to test this with.
          </p>

          <p><code>
          if (password_verify($password, $hash)) {<br>
             &nbsp;&nbsp;   if (password_needs_rehash($hash, $algorithm, $options)) {<br>
                    &nbsp;&nbsp; &nbsp;&nbsp; $hash = password_hash($password, $algorithm, $options);<br>
                    &nbsp;&nbsp; &nbsp;&nbsp; /* Store new hash in db */<br>
             &nbsp;&nbsp;   }<br>
            }
          </code></p>
        </div>   
      </div>

    <?php require_once 'footer.php';?>