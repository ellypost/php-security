<hr>
<p>Your session ID: <?=session_id();?></p>

<p>Hijack demo: visit the insecure private page and run the following JS in your console:<br>
<code>document.cookie="PHPSESSID={other_id}"</code>
<br>
where <code>{other_id}</code> is the session ID you want to hijack.</p>

<ul>
	<li><a href="logout.php">Logout</a></li>
	<li><a href="login.html">Login</a></li>
	<li><a href="private.php">Insecure Private Page</a></li>
	<li><a href="private-semi-secure.php">Semi-secure Private Page</a></li>
</ul>